import { Link, Route, Switch } from 'react-router-dom'
import Home from './Home'
import About from './About'
import Users from './Users'
import MyName from './MyName'
import NotFound from './NotFound'

import Categories from '../Pages/Categories'
import Products from '../Pages/Products'
import ProductDetails from '../Pages/ProductDetails'


export default function Content() {
    return (
        <Switch>
            <Route path='/' component={Categories} exact={true} />  {/* http://localhost:3000/ */}
            <Route path='/category/' component={Products} />  {/* http://localhost:3000/about */}
            <Route path='/productdetails/:productid' >
                <ProductDetails /> { /* children usefull for pass props */}
            </Route> {/* http://localhost:3000/users */}
            <Route path='*' component={NotFound} />
        </Switch>
    );
}

// export default function Content() {
//     return (
//         <Switch>
//             <Route path='/' component={Home} exact={true} />  {/* http://localhost:3000/ */}
//             <Route path='/about' component={About} />  {/* http://localhost:3000/about */}
//             <Route path='/users/:userid' >
//                 <Users /> { /* children usefull for pass props */}
//             </Route> {/* http://localhost:3000/users */}
//             <Route path='/myname' component={MyName} />  {/* http://localhost:3000/myname */}
//             <Route path='*' component={NotFound} />
//         </Switch>
//     );
// }

import { useParams } from 'react-router-dom'

export default function Users() {
    const { userid } = useParams()

    console.log(userid)

    return <div>
        { userid 
            ? `Hello ${userid}`
            : 'Hello anonimous'
        }
        </div>
}

import { useState, useEffect } from 'react';
import axios from 'axios'

export default function Home() {

    const [ categories, setCategories] = useState([])
    function getCategories() {
        axios.get('https://fakestoreapi.com/products/categories')
            .then(res=>setCategories(res.data))
        // fetch('https://fakestoreapi.com/products/categories')
        //     .then(res=>res.json())
        //     .then(json=>setCategories(json))
    }
    useEffect(getCategories,[])

    return <div>
        { categories.map(e=> {
            console.log(e)
            return <Category name={e}/>
        }) }
    </div>
}

function Category({ name }) {
    return <h1>{name}</h1>
}
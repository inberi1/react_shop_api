import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom'
import axios from 'axios'

export default function ProductDetails() {
    const { productid } = useParams()
    const [ productDetails, setProductDetails] = useState()
    function getProductDetails() {
        axios.get('https://fakestoreapi.com/products/'+productid)
            .then(res=>setProductDetails(res.data))
    }
    useEffect(getProductDetails,[])
    return <div className='product'>
                <h1>{'Loading'}</h1>
        </div>
}

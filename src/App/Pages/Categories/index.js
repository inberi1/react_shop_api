import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios'

export default function Categories() {
    const [ categories, setCategories] = useState([])
    function getCategories() {
        axios.get('https://fakestoreapi.com/products/categories')
            .then(res=>setCategories(res.data))
    }
    useEffect(getCategories,[])
    return <div className='box'> { 
        categories.map(e=>
            <div className='link'>
                <Link key={e} to={'/category/'+e}>{e}</Link>
            </div>)
            }
        </div>
}

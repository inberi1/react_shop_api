import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios'

export default function Products() {
    const [ products, setProducts] = useState([])
    function getProducts() {
        axios.get('https://fakestoreapi.com/products/category/jewelery')
            .then(res=>setProducts(res.data))
    }
    useEffect(getProducts,[])
    return <div className='box'> { 
        products.map(e=> 
            <div className='link'>
                <Link key={e.id} to={'/productdetails/'+e.id}>{e.title}</Link>
            </div>)
            }
        </div>
}

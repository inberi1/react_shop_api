import logo from '../logo.svg';
import './Header.css';
import { useHistory } from 'react-router-dom'
export default function Header() {

    const history = useHistory()

    function goHome() {
        console.log('goHome')
        history.push('/')
    }

    return (
        <header className='header' onClick={goHome}>
            {/* <h1>AAA</h1> */}
            {/* <img src='./photo.png'/> */}
            <img src={logo} className="App-logo" alt="logo" />
            <h1>Not connected 1</h1>
        </header>
    );
}

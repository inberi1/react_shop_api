import { NavLink } from 'react-router-dom'

export default function Menu() {

    const list = [
        { to: '/', exact:true, innnerText: ' Categories ' },
        { to: '/products', exact:false, innnerText: ' Products ' },
        { to: '/productdetails', exact:false, innnerText: ' ProductDetails ' },
    ]
    function componentList() {
        return list.map(e =><NavLink key={e.to} to={e.to} exact={e.exact}>{e.innnerText}</NavLink>) 
    }

    return (
        <nav>{ componentList() }</nav>
    );
}

// export default function Menu() {
//     const list = [
//         { to: '/', exact:true, innnerText: ' Categories ' },
//         { to: '/products', exact:false, innnerText: ' Products ' },
//         { to: '/productdetails', exact:false, innnerText: ' ProductDetails ' },
//     ]
//     return (
//         <nav>
//             <NavLink to={list[0].to} exact={list[0].exact1}>{list[0].innnerText}</NavLink>
//             <NavLink to={list[1].to} exact={list[1].exact1}>{list[1].innnerText}</NavLink>
//             <NavLink to={list[2].to} exact={list[2].exact1}>{list[2].innnerText}</NavLink>
//         </nav>
//     );
// }

// export default function Menu() {
//     return (
//         <nav>
//             <NavLink to='/' exact={true}> Categories </NavLink>
//             <NavLink to='/products'> products </NavLink>
//             <NavLink to='/productdetails'> Productdetails </NavLink>
//         </nav>
//     );
// }

// export default function Menu() {
//     return (
//         <nav>
//             <NavLink to='/' exact={true}> Home </NavLink>
//             <NavLink to='/about'> About </NavLink>
//             <NavLink to='/users'> Users </NavLink>
//             <NavLink to='/myname'> MyName </NavLink>
//             <NavLink to='/blog'> Blog </NavLink>
//         </nav>
//     );
// }
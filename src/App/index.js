import './app.css'
import Header from './Header'
import Menu from './Menu'
import Content from './Content'

function App() {
    return (
        <div className='App'>
            <Header />
            <main>
                <Menu />
                <Content />
            </main>
            <footer>footer</footer>
        </div>
    );
}

export default App
